---
layout: handbook-page-toc
title: Select Partners
description: "Support specific information for select partners"
---

Select partners are by invitation only and are reserved for partners that make
a greater investment in GitLab expertise, develop services practices around
GitLab and are expected to drive greater GitLab product recurring revenues.

## Contacting Support

Select Partners contact us via the [support portal](https://support.gitlab.com).
To help them route properly, they use
[this specialized form](https://support.gitlab.com/hc/en-us/requests/new?ticket_form_id=360000837100).

This should open the ticket in the Zendesk Select Partner form in zendesk. From
here, GitLab Support talks directly to the Partner, not their customer. The
Support Plan will be assumed at Ultimate level.

**Note**: Never associate a customer to an Select Partner's organization, or
vice-versa!

## File uploads

When Select Partners needs to send support files, we have 2 current methods
available to accomodate this:

* Standard ticket uploads (20MB max)
* [Support Uploader](https://about.gitlab.com/support/providing-large-files.html#support-uploader)

## Examples of Support Ticket Submission and Handling for Select Partners

Support provided to Select Partners and their customers will vary depending on
the circumstances under which a ticket is raised. Some examples are:

1. A Select Partner creates a ticket to address a GitLab concern brought to
   them by their customer. The Select Partner will manage the ticket and
   serve as the go-between for the customer and GitLab Support. We will deliver
   **[Priority Support](https://about.gitlab.com/support/#priority-support)
   regardless of the customer's subscription**.

1. A Select Partner is doing commercial work for a customer and raises a
   ticket. They should do so under their own account using a subscription
   they have purchased for themselves. We will deliver support **based on the
   subscription the partner has purchased**.

1. A Select Partner is doing internal training, testing or knowledge
   building. They should raise a ticket under their account using their
   [NFR licences](https://about.gitlab.com/handbook/resellers/#nfr-programpolicy).

These examples are not exhaustive. If in doubt, ask questions about the
situation under which the ticket is being raised.